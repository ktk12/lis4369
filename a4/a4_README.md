# LIS4369: Extensible Enterprise Solutions

## Kyle Thomas Kane

### Assignment 4 # Requirements:

*Twelve parts:*

1. Create Person class (must be stored in discrete file apart from Main() function)
    
2. Create three protected data members:
    + fname 
    + lname 
    + age

3. Create three setter/mutator functions:
    + SetFname
    + SetLname
    + SetAge

4. Create four getter/accesor functions:
    + GetFname
    + GetLname
    + GetAge
    + GetObjectInfo (virtual function (returns strings): allowed derived class to override base class function)

5. Create two constructors:
    + default constructor (accepts no arguments)
    + parameterized constructor that accepts three arguments

6. Instantiate two person objects:
    + one from default constructor (display default, then modify and display new data member values)
    + one from parameterized constructor (passing three arguments to its constructors parameters), display new data member values

7. Create Student class (must be stored in discrete file apart from Main() function)

8. Create three private data members:
    + college 
    + major 
    + gpa 

9. Create three getter/accessor functions:
    + GetName
    + GetFullName
    + GetObjectInfo (demonstrates polymorhpism: one interface, mulptiple purposes or roles)  

10. Create two constructors:
    + default constructor (accepts no arguments)
    + parameterized constructor that accepts six arguments

11. Instantiate two student objects:
    + one from default constructor (display default, then modify and display new data member values)
    + one from parameterized constructor (passing six user-entered arguments to  constructors parameters), display new data member values

12. Allow user to press any key to return back to command line.

 

#### Assignment 4 Screenshots:

*Screenshot of application running*:

![Application Run](http://i.imgur.com/qA6tRRZ.jpg)

*Screenshot of application performing data validation*:

![Data Validation Run](http://i.imgur.com/57px6mb.png)
