# LIS4369: Extensible Enterprise Solutions

## Kyle Thomas Kane

### Project 1 # Requirements:

*Seven parts:*

1. Create Room Class;
2. Create following fields (aka properties of data members):
    + private string type (room type);
    + private double length (room length);
    + private double width (room width);
    + private double height (room height);
3. Create two constructors:
	+ default constructors
    + parameterized constructor that accepts four arguments (for fields above)
4. Create following mutator (aka setter) methods:
    + SetType
    + SetLength
    + SetWidth
    + SetHeight
5. Create following accesor (aka getter) methods:
    + GetType
    + GetLength
    + GetWidth
    + GetHeight
    + GetArea
    + GetVolume
6. Must include the following functionality:
    + display room size calculations in feet (as per below)
    + must include data validation
    + round to two decimal places
7. Allow user to press any key to return back command line.


#### Project 1 Screenshots:

*Screenshot of Project 1 running (with correct user input)*:

![Project 1](http://i.imgur.com/Gbl0g7f.jpg)

*Screenshot of Project 1 w/ data validation*:

![Project 1 w/ data validation](http://i.imgur.com/qYosctg.jpg)

