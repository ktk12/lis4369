# LIS4369 - Extensible Enterprise Soultions

##  Kyle T. Kane

### Assignment 2 # Requirements:

*Five parts:*

1. Backward-engineer (using .NET Core) a Simple Calculator application
2. Application Requirements:
	* Display short assignment requirements
	* Display *your* name as "author"
	* Display current date/time
	* Must perform and display each mathematical operation
3. Optional: Provide application logic that alerts the user indicating that division by zero is not permitted

> 


#### Assignment Screenshots:

*Screenshot of valid operation*:

![Valid Operation](http://i.imgur.com/pqi8DG4.png)

*Screenshot of invalid operation*:

![Invalid Operation](http://i.imgur.com/W4wm5lx.png)



