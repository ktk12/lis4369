# LIS4369: Extensible Enterprise Solutions

## Kyle Thomas Kane

### Assignment 5 # Requirements:

*Six Parts:*

1. Create Vehicle (base/parent/super) class

 * Create two private data members (field/instance variables):
 	+ milesTraveled (float)
 	+ gallonsUsed (float)
 
 * Create four properties:
	+ Manufacturer (string)
	+ Make (string)
	+ Model (string)
	+ MPG (float)
*note*: Only Manufacturer, Make, Model are auto-implemented properties (MPG is read-only)

 * Create two setter/mutator functions:
	+ SetMiles
	+ SetGallons

 * Create four getter/accessor functions:
	+ GetMiles
	+ GetGallons
	+ GetObjectInfo()
	+ GetObjectInfo(arg)  (overloaded function): accepts string arg to be used as seperator for display purposes.
 
 * Create two constructors: 
	+ default constructor (accepts no arguments)
	+ parameterized constructor w/ default parameter values (accpets arguments)

2. Create Car (derived/child/sub) class
 
 * Create on private data member (field/instance variable):
 	+ style (string)

 * Create one property:
	+ Style (string)
*note*: Style property is not auto-implemented
 
 * Create two constructors:
	+ default constructor (accepts no arguments)
	+ parameterized constructor w/ default parameter values (accpets arguments)

 * Create one getter/accessor function:
	+ GetObjectInfo(arg)  (overloaded function): accepts string arg to be used as seperator for display purposes.

3. Instatiate three vehicle objects:
	* one from default constructor, display data member values
	* two from parameterized constructor (passing arguments to its constructor's parameters), display data member values

4. Instatiate one care object:
	* one from parameterized constructor (passing arguments to its constructor's parameters), display data member values

5. Must include data validation for numeric data types

6. Allow user to press any key to return to command line.


#### Assignment Screenshots:

*Screenshot of application running*:

![A5 Running](http://i.imgur.com/p1GjCbg.png)

*Screenshot of application running with data validation*:

![A5 Data Validation](http://i.imgur.com/OoOMMdU.png)

