# LIS4369: Extensible Enterprise Solutions

## Kyle Thomas Kane

### Project 2 # Requirements:

*Three parts:*

1. Prompt user for last name. Return full name, occupation, and age.
2. Prompt user for age and occupation (Dev or Manager). Return full name. (Must include data validation for numeric data types)
3. Allow user to press any key to return to command line.



#### Assignment Screenshots:

*Screenshot of Successful program*:

![Successful program](http://i.imgur.com/YkRw4nw.png)

*Screenshot of data validation*:

![Data validation](http://i.imgur.com/5KZDWGS.png)
