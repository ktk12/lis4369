> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369: Extensible Enterprise Solutions

## Kyle Kane

### Assignment 1 # Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket, and Development Enviroments
2. Development Installations
3. Questions
4. Bitbucket repo links: a) this assignment and b)the completed tutorials above (bitbucketstationlocation and myteamquotes)

#### README.md file should include the following items:

* Screenshot of HWAPP application running
* Screenshot of aspnetcoreapp application running
* Git commands with short descriptions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: This command creates an empty Git repository
2. git status: This command displays the state of the working directory and the staging area
3. git add: This command adds file contents to the index
4. git commit: This command commits the staged snapshot to the project history
5. git push: This command sends your committed changes to bitbucket
6. git pull: This command pulls a file from a remote repo into your local repo
7. git clone: This command creates a copy of a local repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](http://i.imgur.com/GxZnL8y.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](http://i.imgur.com/NgLeEqu.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ktk12/bitbucketstationlocation)

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ktk12/myteamquotes)
