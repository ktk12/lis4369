# LIS4369: Extensible Enterprise Solutions

## Kyle Kane

### LIS4369 # Requirements:

####Course Work Links:

1. [A1 README.md](https://bitbucket.org/ktk12/lis4369/src/09858225071935c0cb536ae3898c859238fa7c82/a1/a1_README.md?at=master&fileviewer=file-view-default)

	* Install .NET Core
	* Create hwapp application
	* Create aspnetcoreapp application
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket Tutorials (bitbucketstationlocation and myteamquotes)
	* Provide git command descriptions

	
2.	[A2 README.md](https://bitbucket.org/ktk12/lis4369/src/09858225071935c0cb536ae3898c859238fa7c82/a2/a2_README.md?at=master&fileviewer=file-view-default)

	* Using .NET Core, backward engineer a console application
	* Create a Simple Calculator program using .NET Core
	* Provide screenshots of console application


3.	[A3 README.md](https://bitbucket.org/ktk12/lis4369/src/7987dd21fd819e8070ce36a7e7bb77a8388a371d/a3/a3_README.md?at=master&fileviewer=file-view-default)

	* Using .NET Core, backward engineer a console application
	* Create a Future Value Calculator
	* Provide screenshots of console application


4.	[A4 README.md](https://bitbucket.org/ktk12/lis4369/src/6463178d93615a7ebca7352fe593a0cda7ff388b/a4/a4_README.md?at=master&fileviewer=file-view-default)

	* Backward-engineer (using .NET Core) the following console application screenshot:
	* Display short assignment requirements.
	* Display *your* name as “author.”
	* Display current date/time (must include date/time, your format preference).
	* Create two classes: person and student 
	* Must include data validation on numeric data.


5.	[A5 README.md](https://bitbucket.org/ktk12/lis4369/src/dfb2715002d953c37cdf1985ab9890a27650c9ed/a5/a5_README.md?at=master&fileviewer=file-view-default)

	* Backward-engineer (using .NET Core) the following console application screenshot:
	* Display short assignment requirements.
	* Display *your* name as “author.”
	* Display current date/time (must include date/time, your format preference).
	* Create two classes: vehicle and car
	* Must include data validation on numeric data.


6.	[P1 README.md](https://bitbucket.org/ktk12/lis4369/src/3c6ecaf8a082a7d8776567f87a0630ff424469db/p1/p1_README.md?at=master&fileviewer=file-view-default)

	* Using .NET Core, backward engineer a console application
	* Display short assignment requirements
	* Display current Date/Time
	* Use classes and objects in C#
	* Must perform and display room size calculations , must inlcude data validation and rounding to two decimal places
	* Each data member must have get/set methods, also GetArea and GetVolume
	* Provide screenshots of successful application and data validation


7.	[P2 README.md](https://bitbucket.org/ktk12/lis4369/src/37629b08068160e66c65760bd1a496bcc4a815dc/p2/p2_README.md?at=master&fileviewer=file-view-default)

	* Complete the following LINQ tutorial
	* Use what you learned in LINQ tutorial to engineer following program
	* Prompt user for Last name, return matching full name, occupation and age
	* Prompt user for occupation and age, return matching full name
	* Must include data validation for numeric data types
	* Allow user to return to command line with any key
	* Provide screenshots of successful application and data validation