

# LIS4369 - Extensible Enterprise Soultions

##  Kyle T. Kane

### Assignment 2 # Requirements:

*Seven Parts:*

1. Display short assignment requirements
2. Disply name as author
3. Display current date/time 
4. Must perform and display future value calculation
5. Must include data validation
6. Must use decimal data type for all currency variables
7. Use intrinsic currency format to display future value calculation



#### Assignment Screenshots:

*Screenshot of console application with data validation*:

![AMPPS Installation Screenshot](http://i.imgur.com/41N7pJa.png)


